package com.oolik.SchedulerApp;

/**
 * <p>
 *     This interface is implemented by <i>TutorialEvent </i> and <i>LectureEvent</i>
 *     classes.
 */
public interface ISchedule {

    void printAlert();

    String toString();

}
