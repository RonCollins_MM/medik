package com.oolik.SchedulerApp;

import java.util.Scanner;

/**
 * Main class.
 * <p>
 *     This is the major class involved in manipulation of the <i>ISchedule</i> array.
 *     Adds events as well as prints alerts and does display of the events in the array.
 */
public class Scheduler {

    //variables
    private MainMenu menu;

    //Constructors
    public Scheduler() {
        menu = new MainMenu();
    }

    //Sub modules
    private boolean isArrayFull(ISchedule[] events, int numberOfEvents) {

        int eventsInArray = 0;
        for (int i = 0; i < numberOfEvents; i++) {
            if (events[i] != null)
                eventsInArray++;
        }
        return eventsInArray >= 10;

    }

    private ISchedule[] addLectureEvent(ISchedule[] events, int numberOfEvents) {

        LectureEvent newEvent = new LectureEvent();

        Scanner myScanner = new Scanner(System.in);
        System.out.println("Enter Unit Name");
        newEvent.setUnitName(myScanner.next());
        System.out.println("Enter Lecture time" +
                "\nHint: Use format <" + "dd/mm/yyyy-HH:mm" + "> to avoid Errors");
        newEvent.setEventTime(myScanner.next());
        System.out.println("Enter Lecture Hall");
        newEvent.setLectureHall(myScanner.next());

        events[numberOfEvents] = newEvent;
        System.out.println("Event added");

        return events;

    }

    private ISchedule[] addTutorialEvent(ISchedule[] events, int numberOfEvents) {

        Scanner myScanner = new Scanner(System.in);

        System.out.println("Enter Tutorial room\nHint: Enter a number between 1 - 400");
        int tutorialRoom = myScanner.nextInt();
        if(tutorialRoom <1 || tutorialRoom >400){
            System.out.println(" Invalid input. Enter a number between 1 and 400 inclusive. ");
            this.addTutorialEvent(events,numberOfEvents);
        }else{
            TutorialEvent newEvent = new TutorialEvent();
            newEvent.setTutorialRoom(tutorialRoom);
            System.out.println("Enter Unit Name");
            newEvent.setUnitName(myScanner.next());
            System.out.println("Enter Tutorial time" +
                    "\nHint: Use format <" + "dd/mm/yyyy-HH:mm" + "> to avoid Errors");
            newEvent.setEventTime(myScanner.next());

            events[numberOfEvents] = newEvent;
            System.out.println("Event added");
        }

        return events;

    }
/**
 * <p>
 *     Method called when a new event is to be added to the array.
 *     Ensures that only 10 event exist in the array at any point in time.
 *     Takes the array to contain the events from the <i>MainMenu class</i> and returns
 *     after the event has been added.
 *     @param events
 *     @param numberOfEvents
 *     @return ISchedule[]
 * */
    public ISchedule[] addEvent(ISchedule[] events, int numberOfEvents) {
        if (isArrayFull(events, numberOfEvents)) {
            System.out.println("Sorry you cannot add any events at this time." +
                    " Maximum of 10 events allowed.");
        } else {
            System.out.println("What kind of Event would you like to add?" +
                    "\n\t\t1. Add LectureEvent\n\t\t2. Add TutorialEvent\n\t\t3. Return to main menu");
            Scanner myScanner = new Scanner(System.in);
            switch (myScanner.nextInt()) {
                case 1:
                    events = this.addLectureEvent(events, numberOfEvents);
                    break;
                case 2:
                    events = this.addTutorialEvent(events, numberOfEvents);
                    break;
                case 3:
                    menu.selectedChoiceHandler(menu.printMenu());
                default:
                    System.out.println("Invalid Input.");
            }
        }

        return events;
    }
/**
 * <p>
 *     Method called when alerts in the array are to be printed.
 *     @param events
 *     @param numberOfEvents
 * */
    public void printAlerts(ISchedule[] events, int numberOfEvents) {
        for (int i = 0; i < numberOfEvents; i++) {
            events[i].printAlert();
        }

    }
    /**
     * <p>
     *     Method called when the events in the array are to be printed.
     *     @param events
     *     @param numberOfEvents
     * */
    public void displayEvents(ISchedule[] events, int numberOfEvents) {
        for (int i = 0; i < numberOfEvents; i++) {
            String s = events[i].toString();
            System.out.println(s);
        }

    }
}