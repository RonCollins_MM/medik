package com.oolik.SchedulerApp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Child class for UniEvents.
 * <p>
 *     Responsible for creation of LectureEvents
 */
public class LectureEvent extends UniEvents implements ISchedule {

    //fields
    private String lectureHall;
    private Date date;

    //constructors
    public LectureEvent() {

        this.lectureHall = "Not Specified";
        this.unitName = "Not Specified";
        this.eventTime = "01/01/1970-00:00";

    }

    public LectureEvent(String newLectureHall, String newUnitName, String newEventTime) {

        this.lectureHall = newLectureHall;
        this.unitName = newUnitName;
        this.eventTime = newEventTime;

    }

    //Accessors
    public String getLectureHall() {
        return lectureHall;
    }

    public String getUnitName() {
        return unitName;
    }

    public String getEventTime() {
        return eventTime;
    }

    //Mutators
    public void setLectureHall(String lectureHall) {
        this.lectureHall = lectureHall;
    }

    public void setEventTime(String newEventTime) {
        this.eventTime = newEventTime;
    }

    public void setUnitName(String newUnitName) {
        this.unitName = newUnitName;
    }

    //Sub-modules


    //TODO Work on this method
    @Override
    boolean equals(ISchedule event) {
        return false;

    }

    public String toString() {
        return "Lecture on " + this.unitName + ", date: " + this.eventTime + " in " + this.lectureHall;
    }


    /**
     * <p>
     *     Method called when alerts for objects from this class are to be printed.
     *     Calculates the time difference between the scheduled time and current time
     * */
    @Override
    public void printAlert() {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm");

        try {
            date = simpleDateFormat.parse(this.eventTime);
        } catch (ParseException e) {
            System.out.println(e.toString());
        }

        long difference = date.getTime() - System.currentTimeMillis();

        if (difference < 0) {
            System.out.println("The Lecture: " + this.unitName
                    + " set for " + this.eventTime + " has expired !");
        } else {
            long seconds = 1000;
            long minutes = seconds * 60;
            long hours = minutes * 60;
            long days = hours * 24;

            long daysLeft = difference / days;
            difference %= days;
            long hoursLeft = difference / hours;
            difference %= hours;
            long minutesLeft = difference / minutes;

            System.out.println("You have a lecture in " + this.lectureHall +" for "+ this.unitName
                    + " in " + daysLeft + " days, " + hoursLeft + " hours and " + minutesLeft
                    + " minutes !");

        }

    }

}
