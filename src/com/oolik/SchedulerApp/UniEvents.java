package com.oolik.SchedulerApp;

/**
 * <p>
 * Parent class for all events including the Tutorial and Lecture events.
 */

public abstract class UniEvents {

    protected String unitName;
    protected String eventTime;


    abstract boolean equals(ISchedule event);

}
