package com.oolik.SchedulerApp;

/**
 * <p>
 *     Launcher class.
 * <p>
 *     Contains the Main method. This is the starting point of the application.
 */
public class Launcher {

    public static void main(String[] args) {

        MainMenu menu = new MainMenu();
        menu.selectedChoiceHandler(menu.printMenu());
    }
}
