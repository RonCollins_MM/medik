package com.oolik.SchedulerApp;

import java.util.Scanner;

/**
 * <p>
 * Class dedicated to handling the menu and the input form the user.
 */
public class MainMenu {

    //variables
    private ISchedule[] events;
    private int numberOfEvents;

    public MainMenu() {
        events = new ISchedule[15];
        numberOfEvents = 0;
    }
/**
 * Presents the menu to the user and handles the input from the user.
 * Called by the launcher class in the start of the program.
 * */
    public int printMenu() {

        System.out.println("What would you like to do ?" +
                "\n\t\t1. Add an Event\n\t\t2. Display Events\n\t\t3. Print Alert\n\t\t4. Exit");

        Scanner myScanner = new Scanner(System.in);

        return myScanner.nextInt();
    }

    public void selectedChoiceHandler(int selectedChoice) {

        Scheduler scheduler = new Scheduler();

        switch (selectedChoice) {
            case 1:
                events = scheduler.addEvent(events, numberOfEvents);
                numberOfEvents++;
                this.selectedChoiceHandler(printMenu());
                break;
            case 2:
                scheduler.displayEvents(events, numberOfEvents);
                this.selectedChoiceHandler(printMenu());
                break;
            case 3:
                scheduler.printAlerts(events, numberOfEvents);
                this.selectedChoiceHandler(printMenu());
                break;
            case 4:
                System.out.println("\nThank you for using SchedulerApp 1.0." +
                        "\n\t\tSchedulerApp will now Exit....");
                System.exit(0);
                break;
            default:
                System.out.println("Invalid Input");
        }

    }

}