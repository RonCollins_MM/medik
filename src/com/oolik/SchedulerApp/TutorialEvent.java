package com.oolik.SchedulerApp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Child class for UniEvents.
 * <p>
 *     Responsible for creation of TutorialEvents
 */
public class TutorialEvent extends UniEvents implements ISchedule {

    //fields
    private int tutorialRoom;
    private Date date;

    //constructors
    public TutorialEvent() {

        this.tutorialRoom = 0 ;
        this.unitName = "Not Specified";
        this.eventTime = "01/01/1970-00:00";

    }

    public TutorialEvent(int newTutorialRoom, String newUnitName, String newEventTime) {

        this.tutorialRoom = newTutorialRoom;
        this.unitName = newUnitName;
        this.eventTime = newEventTime;

    }

    //Accessors
    public int getTutorialRoom() {
        return tutorialRoom;
    }

    public String getUnitName() {
        return unitName;
    }

    public String getEventTime() {
        return eventTime;
    }

    //Mutators
    public void setTutorialRoom(int tutorialRoom) {
        this.tutorialRoom = tutorialRoom;
    }

    public void setEventTime(String newEventTime) {
        this.eventTime = newEventTime;
    }

    public void setUnitName(String newUnitName) {
        this.unitName = newUnitName;
    }

    //Sub-modules


    //TODO Work on this method
    @Override
    boolean equals(ISchedule event) {
        return false;

    }

    public String toString() {
        return "Tutorial: " + this.unitName + ", date: " + this.eventTime
                + " in " + this.tutorialRoom;
    }

    /**
     * <p>
     *     Method called when alerts for objects from this class are to be printed.
     *     Calculates the time difference between the scheduled time and current time
     * */
    @Override
    public void printAlert() {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm");

        try {
            date = simpleDateFormat.parse(this.eventTime);
        } catch (ParseException e) {
            System.out.println(e.toString());
        }

        long difference = date.getTime() - System.currentTimeMillis();

        if (difference < 0) {
            System.out.println("The Tutorial: " + this.unitName
                    + " set for " + this.eventTime + " has expired !");
        } else {
            long seconds = 1000;
            long minutes = seconds * 60;
            long hours = minutes * 60;
            long days = hours * 24;

            long daysLeft = difference / days;
            difference %= days;
            long hoursLeft = difference / hours;
            difference %= hours;
            long minutesLeft = difference / minutes;

            System.out.println("You have a Tutorial in " + this.tutorialRoom+" for "+ this.unitName
                    + " in " + daysLeft + " days, " + hoursLeft + " hours and " + minutesLeft
                    + " minutes !");

        }

    }

}
